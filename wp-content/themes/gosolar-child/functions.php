<?php
/* =========================================
 * Enqueues child theme stylesheet
 * ========================================= */

function gosolar_zozo_enqueue_child_theme_styles() {
	wp_enqueue_style( 'gosolar-zozo-child-style', get_stylesheet_uri(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'gosolar_zozo_enqueue_child_theme_styles', 30 );

